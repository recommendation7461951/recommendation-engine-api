openapi: "3.0.1"
info:
  title: "Recommendation Engine API"
  version: "-"
  description: "This API is to be called by a Recommendation Service to return a set of product recommendations based on an input set.  The request must contain a set of recommenders with weights that add up to 1.0 (100%), which determines the number of results collected from each recommender."


tags:
  - name: "Recommendation"
    description: "Retrieve product recommendations"
  - name: "Product Similarity SKU Insertion"
    description: "Insert new SKU into Similarity model"

paths:
  /recommendation:
    post: 
      tags:
        - Recommendation
      description: Request for product recommendation based on input set of products and/or customer ID/trending options
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/recommendation-req"
            examples:
              sample-request-1:
                $ref: "#/components/examples/recommendation-req-with-customer"
              sample-request-2:
                $ref: "#/components/examples/recommendation-req-with-customer-personal-info"
              sample-request-3:
                $ref: "#/components/examples/recommendation-req-without-customer"
              sample-request-4:
                $ref: "#/components/examples/recommendation-req-without-customer-result-size"
              sample-request-5:
                $ref: "#/components/examples/recommendation-req-without-customer-with-client-id"
              sample-request-6:
                $ref: "#/components/examples/recommendation-req-trend"
      responses:
        201:
          description: Successful request
          content:
            application/json :
              schema:
                $ref: "#/components/schemas/recommendation-result"
              examples:
                result-example-1:
                  $ref: "#/components/examples/recommendation-result"
                result-example-2:
                  $ref: "#/components/examples/recommendation-result-with-model-scores"
                result-example-3:
                  $ref: "#/components/examples/recommendation-result-with-client-id"
                result-example-4:
                  $ref: "#/components/examples/recommendation-result-2-recommenders-called-with-1-returning-error"
                result-example-5:
                  $ref: "#/components/examples/recommendation-result-2-recommenders-called-with-2-returning-error"
                
        400:
          description: Request is invalid.
          content:
            application/json : {}
        422:
          description: Validation error
          content:
            application/json : 
              examples:
                error-example-1:
                  $ref: "#/components/examples/error-result-weightage"      
                error-example-2:
                  $ref: "#/components/examples/error-result-weightage-and-missing-field"      
                  

  /insert-sku:
    post: 
      tags:
        - Product Similarity SKU Insertion
      description: Insertion of new SKUs into running product-similarity model only
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/similarity-insert-req"
            examples:
              sample-request-1:
                $ref: "#/components/examples/similarity-insert-req"
            
      responses:
        201:
          description: Successful request
          content:
            application/json :
              schema:
                $ref: "#/components/schemas/similarity-insert-result"
              examples:
                result-example-1:
                  $ref: "#/components/examples/similarity-insert-result"
                result-example-2:
                  $ref: "#/components/examples/similarity-insert-result-existing-sku"
                result-example-3:
                  $ref: "#/components/examples/similarity-insert-result-ignored-sku"
                result-example-4:
                  $ref: "#/components/examples/similarity-insert-result-warning"
                
        400:
          description: Request is invalid.
          content:
            application/json : {}
        422:
          description: Validation error
          content:
            application/json : 
              examples:
                error-example-1:
                  $ref: "#/components/examples/error-similarity-number-of-productSKU-and-productNames-do-not-match"      


components:
  schemas:
    recommendation-req:
      title: Recommendation request
      type: object
      properties:
        productSKU:
          description:  Array of product SKUs for purchased products
          type: array
          items: 
            type: string
        storeId:
          type: string
          description:  Unique ID for the Store
        weightage:
          description: Weightage for results across the recommender systems that must add up to 1.0 (100%)
          type: array
          items:
            type: object
            properties:
              recommender:
                type: string
                enum:
                  - sml
                  - asc
                  - trd
                  - adl
                  - ucf
              weight:
                type: number
                format: float
        customerId:
          type: string
          description: Unique ID for the customer (required for Customer-personalised recommendations)
        clientId:
          type: string
          description:  Unique ID for the client company for which to produce the recommendation for
        outletFilter:
          type: boolean
          default: false
          description: If true, will filter and return result only from the specified store id. Else, it return result from all stores. Use a true boolean value instead of string "true"
        resultSize:
          type: integer
          format: int32
          description:  Optional - Number of product recommendations to return.  Defaults to 10.
          default: 10
          minimum: 1
          maximum: 50
        displayScore:
          type: boolean
          default: false
          description:  Display model score metrics for each sku recommended. Note that "ucf" recommender does not have a score metric.
        trending:
          description: Options only if trending recommender `"trd"` is called
          type: object
          properties:
            allStores:
              type: boolean
              default: false
              description: If true, will return trending products from all stores. Else, it will use "storeId" value to compute trending for the particular store
            category:
              type: string
              description: Product type name, or "all" to search for trending items regardless of product type
            date:
              type: string
              description: Date in "YYYY-MM-DD", or "today"
            termAverage:
              type: array
              description: Days of short and long term averages (from date field). e.g. [3,7] means short term average of 3 days, and long term average of 7 days.
              items:
                type: integer
            birthYear:
              type: integer
              description: birth year of customer. Must be 4 digits.
        personalised:
          description: Demographics information for personalised recommenders ("ucf" and "adl"). Highly encouraged to provide the specific fields that these recommenders are trained with, or the recommendations might be less accurate. In the case of a customer having an invalid value, e.g., postalSector out of range, the recommender will return a less accurate recommendation, or None, depending on the model. 
          type: object
          properties:
            postalSector:
              type: integer
              description: The first two digits of the postal code of customer.
            birthYear:
              type: integer
              description: birth year of customer. Must be 4 digits.
            gender:
              type: string
              description: Gender of customer. `"m"` or `"f"`.

      required:
        - products
        - storeId
        - weightage
        
    recommendation-result:
      title: Recommendation result
      type: object
      properties:
        requestId:
          type: string
          description:  Backend-generated UID for the result
        storeId:
          type: string
          description:  Echoed Store ID as sent in the Request
        customerId:
          type: string
          description:  Echoed Customer ID, if it was provided in the Request. Otherwise, returns null.
        clientId:
          type: string
          description:  Echoed Client ID, if it was provided in the Request. Otherwise, returns null.
        products:
          description:  Array of recommended products
          type: array
          items: 
            type: object
            properties:
              recommendationUID:
                type: string
                description: Backend-generated UID for the product recommendation.  The UID is different for each call
              sku:
                type: string
                description: Additional UID for the product as defined by the Technology Partner.  May be an internal product ID in the product catalog.   
              score:
                type: number
                format: float
                description: model score. Each model have different scoring metrics.        
            required:
              - recommendationUID
              - sku
        errorMessages:
          description: Array of error Messages(if any) returned by recommenders
          type: array
          items:
            type: object
            properties: 
              recommender_name:
                type: string
                description: name of the recommender and its error message

      required:
        - requestId
        - products
        - storeId

    similarity-insert-req:
      title: Similarity insertion request
      type: object
      properties:
        productSKU:
          description:  Array of product SKUs for products to be added
          type: array
          items: 
            type: string
        productNames:
          description:  Array of product SKUs names for products to be added
          type: array
          items: 
            type: string
        storeId:
          type: string
          description:  Unique ID for the Store
      required:
        - productSKU
        - productNames
        - storeId

    similarity-insert-result:
      title: Similarity insertion result
      type: object
      properties:
        existing_sku:
          description: Array of SKUs NOT added as they already exist in the model
          type: array
          items:
            type: string
        ignored_sku:
          description: Array of SKUs NOT added as their titles are not valid
          type: array
          items:
            type: string
        inserted_sku: 
          description: Array of SKUs added into product similarity model  
          type: array
          items:
            type: string
        warning:
          description: Warning message if new products are added but changes are not live
          type: string
      required:
        - existing_sku
        - ingnored_sku
        - inserted_sku

    
  examples:
    recommendation-result:
      summary: "Recommendation Result"
      value: {
          "requestId" : "1617730918",
          "storeId" : "AMK6675",
          "customerId" : "GGTF5467RF7",
          "clientId" : null,
          "products" : [
            {
              "recommendationUID" : "ASC-1617730918-1",
              "sku" : "ABC1561232"
            },
            {
              "recommendationUID" : "SML-1617730918-1",
              "sku" : "ABC1561555"
            },
            {
              "recommendationUID" : "UCF-1617730918-1",
              "sku" : "TRT1561232"
            }
          ]
      }
    recommendation-result-2-recommenders-called-with-1-returning-error:
      summary: "Recommendation Result of calling 2 recommenders, 1 returning an error"
      value: {
          "requestId" : "1617730918",
          "storeId" : "AMK6675",
          "customerId" : "GGTF5467RF7",
          "clientId" : null,
          "products" : [
            {
              "recommendationUID" : "ASC-1617730918-1",
              "sku" : "ABC1561232"
            },
            {
              "recommendationUID" : "ASC-1617730918-1",
              "sku" : "ABC1561555"
            },
            {
              "recommendationUID" : "ASC-1617730918-1",
              "sku" : "TRT1561232"
            }
          ],
          "errorMessages": [{"sml" : "Traceback (most recent call last)...."}]
      }
    recommendation-result-2-recommenders-called-with-2-returning-error:
      summary: "Recommendation Result of calling 2 recommenders, 2 returning an error"
      value: {
          "requestId" : "1617730918",
          "storeId" : "AMK6675",
          "customerId" : "GGTF5467RF7",
          "clientId" : null,
          "products" : [],
          "errorMessages": [{"sml": "Traceback (most recent call last)...."},{"asc": "Traceback (most recent call last)...."}]
      }
    recommendation-result-with-customer:
      summary: "Personalised Recommendation Result"
      value: {
          "requestId" : "1617730918",
          "storeId" : "AMK6675", 
          "customerId" : "GGTF5467RF7",
          "clientId" : null,
          "products" : [
            {
              "recommendationUID" : "ASC-1617730918-1",
              "sku" : "ABC1561232"            },
            {
              "recommendationUID" : "SML-1617730918-1",
              "sku" : "ABC1561555"
            },
            {
              "recommendationUID" : "UCF-1617730918-1",
              "sku" : "TRT1561232"
            }
          ]
      }
    recommendation-result-with-model-scores:
      summary: "Recommendation Result with Model Scores"
      value: {
          "requestId" : "1617730918",
          "storeId" : "AMK6675", 
          "customerId" : "GGTF5467RF7",
          "clientId" : null,
          "products" : [
            {
              "recommendationUID" : "SML-1617730918-1",
              "sku" : "ABC1561232",
              "score": 0.9937          
            },
            {
              "recommendationUID" : "SML-1617730918-2",
              "sku" : "ABC1561555",
              "score": 0.9890
            },
            {
              "recommendationUID" : "SML-1617730918-3",
              "sku" : "TRT1561232",
              "score": 0.9887
            }
          ]
      }       
    recommendation-result-with-client-id:
      summary: "Recommendation Result with Client ID"
      value: {
          "requestId" : "1617730918",
          "storeId" : "AMK6675",
          "customerId" : "GGTF5467RF7",
          "clientId" : "Client-123",
          "products" : [
            {
              "recommendationUID" : "ASC-1617730918-1",
              "sku" : "ABC1561232"
            },
            {
              "recommendationUID" : "SML-1617730918-1",
              "sku" : "ABC1561555"
            },
            {
              "recommendationUID" : "UCF-1617730918-1",
              "sku" : "TRT1561232"
            }
          ]
      }
    error-result-weightage:
      summary: "Weightage does not add up to 1"
      value: [
          {
              "loc": [
                  "weightage"
              ],
              "msg": "sum of weightage is not equals to 1",
              "type": "value_error"
          }
      ]     
    error-result-weightage-and-missing-field:
      summary: "Weightage does not add up to 1 and missing mandatory field"
      value: [
          {
              "loc": [
                  "storeId"
              ],
              "msg": "field required",
              "type": "value_error.missing"
          },
          {
              "loc": [
                  "weightage"
              ],
              "msg": "sum of weightage is not equals to 1",
              "type": "value_error"
          }
      ]    
      
    recommendation-req-with-customer:
      summary: "Personalised Recommendation request and display model scores"
      value: {
        "productSKU" : [ "TTYR4521ED", "TR5US4521ED"],
        "storeId" : "AMK6675",
        "customerId" : "GGTF5467RF7",
        "displayScore" : true,
        "weightage" : [
            { "recommender" : "sml", "weight" : 0.5 },
            { "recommender" : "asc", "weight" : 0.4 },
            { "recommender" : "ucf", "weight" : 0.1 }
          ]
      }

    recommendation-req-with-customer-personal-info:
      summary: "Personalised Recommendation request with personal information"
      value: {
        "productSKU" : [ "TTYR4521ED", "TR5US4521ED"],
        "storeId" : "AMK6675",
        "customerId" : "GGTF5467RF7",
        "weightage" : [
            { "recommender" : "sml", "weight" : 0.5 },
            { "recommender" : "asc", "weight" : 0.4 },
            { "recommender" : "ucf", "weight" : 0.1 }
          ],
        "personalised": {
            "postalSector": "54",
            "birthYear": 1975,
            "gender": "f"
        }
      }

    recommendation-req-without-customer:
      summary: "Recommendation request"
      value: {
        "productSKU" : [ "TTYR4521ED", "TR5US4521ED"],
        "storeId" : "AMK6675",
        "weightage" : [
            { "recommender" : "sml", "weight" : 0.6 },
            { "recommender" : "asc", "weight" : 0.4 }
          ]
      }    
    recommendation-req-without-customer-result-size:
      summary: "Recommendation request with defined result size"
      value: {
        "productSKU" : [ "TTYR4521ED", "TR5US4521ED"],
        "storeId" : "AMK6675",
        "weightage" : [
            { "recommender" : "sml", "weight" : 0.5 },
            { "recommender" : "asc", "weight" : 0.5 }
          ],
        "resultSize" : 15
      } 
    recommendation-req-without-customer-with-client-id:
      summary: "Recommendation request with client ID"
      value: {
        "productSKU" : [ "TTYR4521ED", "TR5US4521ED"],
        "storeId" : "AMK6675",
        "clientId" : "Client-123",
        "weightage" : [
            { "recommender" : "sml", "weight" : 0.6 },
            { "recommender" : "asc", "weight" : 0.4 }
          ]
      }        
    recommendation-req-trend:
      summary: "Recommendation request for product trending"
      value: {
        "productSKU" : [],
        "storeId" : "AMK6675",
        "weightage" : [
            { "recommender" : "trd", "weight" : 1.0 }
          ],
        "resultSize" : 15,
        "trending": {
            "allStores": true,
            "category": "all",
            "date": "2020-02-09",
            "termAverage": [3,7],
            "birthYear": 1975
        }
      } 

    similarity-insert-req:
      summary: "SKU insertion request for product similarity"
      value: {
        "productSKU": ["NEW-SKU"],
        "productNames": ["HOT COCA MILK WITH LESS SUGAR 500G"],
        "storeId": "AMK631"
      }

    similarity-insert-result:
      summary: "Successfully inserted SKU result"
      value: {
        "existing_sku": [],
        "ignored_sku": [],
        "inserted_sku": ["NEW-SKU"]
      }

    similarity-insert-result-existing-sku:
      summary: "SKU already exists in model"
      value: {
        "existing_sku": ["EXISTING-SKU"],
        "ignored_sku": [],
        "inserted_sku": [],
        "errorMessage": "Exception: ['EXISTING-SKU'] already exists in the model."
      }

    similarity-insert-result-ignored-sku:
      summary: "SKUs productNames is invalid, SKU is not inserted to the model"
      value: {
        "existing_sku": [],
        "ignored_sku": ["INVALID-SKU"],
        "inserted_sku": [],
        "errorMessage": "Exception: ['INVALID-SKU'] are NOT inserted to the model, please use a different title."
      }

    similarity-insert-result-warning:
      summary: "Successfully inserted SKU but changes are not live"
      value: {
        "existing_sku": [],
        "ignored_sku": [],
        "inserted_sku": ["NEW-SKU"],
        "errorMessage": "Exception: New Master failed to start, products are inserted but changes are not live. Please restart the docker container"
      }

    error-similarity-number-of-productSKU-and-productNames-do-not-match:
      summary: "Number of items in productSKU and productNames list are not the same "
      value: {
              "loc": [
                  "productNames"
              ],
              "msg": "Number of productNames and productSKU do not match",
              "type": "value_error"
          }

