import json
import requests

from math import ceil


def call_recommender(api_urls, recommender_nm, weight, data):
    """call individual recommenders to get predictions and any error messages"""
    url = api_urls[recommender_nm]
    result_each = ceil(weight * data["resultSize"])
    prediction = requests.post(url, json=data).content
    prediction = json.loads(prediction)
    error_message = prediction.get("errorMessage")
    if error_message:
        recommender_error_msg = {recommender_nm: error_message}
    else:
        recommender_error_msg = None
    return (recommender_nm, prediction.get("sku"), prediction.get("score"), result_each, recommender_error_msg)


def insert_sku_similarity(api_urls, data):
    """Call Similarity API and insert SKU"""
    url = api_urls["sml-insert"]
    insert_sku_res = requests.post(url, json=data).content
    insert_sku_res = json.loads(insert_sku_res)
    return insert_sku_res


def format_errors(concat_list):
    """Check for and seperate any error messages in the recommenders into a different list
    Returns the error messages array and the modified concat list
    """
    error_message_array = []
    # Move errorMessages to new array while returning the modified array
    for i, recommender_tup in enumerate(concat_list):
        if recommender_tup[-1]:
            error_message_array.append(recommender_tup[-1])
        concat_list[i] = recommender_tup[:-1]
    return error_message_array, concat_list