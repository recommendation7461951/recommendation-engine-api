import multiprocessing
import os


# assign workers
max_processes = (multiprocessing.cpu_count() * 2) + 1
try:
    w = os.environ["WORKERS"]
    w = w.lower()
    if w.isnumeric():
        if int(w) > max_processes:
            w = 'max'
except:
    w = 'max'
workers = max_processes if w == 'max' else w


# other configs
timeout = 10
loglevel = "info"
