"""main api to fuse results from each recommenders"""  

import json
import logging
import os
import sys
import traceback
from functools import wraps
from time import time
from typing import List, Optional

import yaml
# from flask_cors import CORS
from flask import Flask, abort, jsonify
from flask import logging as flog
from flask import make_response, request
from fusion import duplicate_removal, fusion, set_product_list_schema
from joblib import Parallel, delayed
from mapping import agegroup_map, personalised_none_handling, region_map
from pydantic import (BaseModel, StrictBool, ValidationError, confloat, conint,
                      constr, root_validator, validator)
from utils_app import *

app = Flask(__name__)
# CORS(app, origins=['http://localhost'])


# change flask log default to same format as gunicorn
format_ = "[%(asctime)s] [%(process)d] [%(levelname)s] in %(module)s: %(message)s"
flog.default_handler.setFormatter(logging.Formatter(format_))

# load mapping tables
if "pytest" not in sys.modules:
    region_mapping = json.load(open("data/region.json"))
    age_mapping = json.load(open("data/agegroup.json"))["agegroup"]

# get configs from yaml, if not using docker-compose
try:
    api_urls_all = json.loads(os.environ["API_URL"])
except:
    cf = yaml.safe_load(open("config.yml"))
    api_urls_all = cf["api_url"]
try:
    default_resultSize = int(os.environ["RESULT_SIZE"])
except:
    cf = yaml.safe_load(open("config.yml"))
    default_resultSize = cf["resultSize"]


# Check if client switching is enabled
client = list(api_urls_all.keys())[0]
if isinstance(api_urls_all[client], dict):
    clientswitch = True
else:
    clientswitch = None


class _weightage(BaseModel):
    recommender: str
    weight: confloat(ge=0, le=1)


class _trending(BaseModel):
    allStores: Optional[bool] = False
    category: Optional[str]
    date: Optional[str]
    termAverage: Optional[List[int]]
    birthYear: Optional[int]

    @validator("termAverage")
    def list_2_int(cls, v):
        if len(v) != 2:
            raise ValueError("There must be 2 integers in the list")


class _personalised(BaseModel):
    postalSector: Optional[str]
    birthYear: Optional[int]
    gender: Optional[str]


class RequestSchema(BaseModel):
    productSKU: List[str]
    storeId: str
    outletFilter: Optional[StrictBool]
    clientId: Optional[str]
    customerId: Optional[str]
    displayScore: Optional[bool]
    resultSize: Optional[conint(ge=1, le=5000)] = default_resultSize
    weightage: List[_weightage]
    trending: Optional[_trending]
    personalised: Optional[_personalised]

    @root_validator(pre=True)
    def clientid_match_config(cls, v):
        """validations for clientId"""
        clientid = v.get("clientId")

        # validate if client switching enabled
        if clientswitch and clientid is None:
            raise ValueError("client-switching enabled, pls input clientId")

        # validate clientId to be same as in config
        if clientid is not None:
            clientid_list = list(api_urls_all.keys())
            if clientid not in clientid_list:
                raise ValueError("clientId '{}' does not match config list of {}".format(clientid, clientid_list))

        # validate trending options are there if trd is called
        for i in v["weightage"]:
            if i.get("recommender"):
                if i["recommender"] == "trd":
                    if v.get("trending") is None:
                        raise ValueError("'trending' inputs are required if trd recommender called")
                    if (not v.get("trending").get("category") or 
                        not v.get("trending").get("date") or 
                        not v.get("trending").get("termAverage")):
                        raise ValueError("'category', 'date', 'termAverage' are required if trd recommender called")
        return v

    @validator("weightage")
    def weight_sum(cls, v, values):
        """multi-conditions validation"""

        # validate weightage sum = 1
        sumw = 0
        for i in range(len(v)):
            weight = v[i].weight
            sumw = weight + sumw
        if sumw != 1:
            raise ValueError("sum of weightage is not equals to 1")

        # get rre names if single / client-switching enabled
        recommender_list = [i.recommender for i in v]
        clientid = values.get("clientId")
        if clientid is not None:
            recommender_list_config = list(api_urls_all[clientid].keys())
        else:
            recommender_list_config = list(api_urls_all.keys())
    
        # validate for customerId if personalised rre called
        # validate recommender names to be same as in config
        for i in recommender_list:
            if (i == "ucf" or i == "adl") and values.get("customerId") is None:
                raise ValueError("customerId required if ucf or adl recommender called")
            if i not in recommender_list_config:
                raise ValueError("'{}' recommender is not in config list of {}".format(i, str(recommender_list_config)))


class InsertSKUSimilaritySchema(BaseModel):
    productSKU: List[constr(max_length=500)]
    storeId: Optional[str]
    clientId: Optional[str]
    customerId: Optional[str]
    productNames: List[constr(max_length=500)]
    
    @validator("productNames")
    def skuNames_match_productSKU(cls, v, values):
        """Ensure same number of product name and product SKU"""
        productnameslen = len(v)
        productSKUlen = len(values.get("productSKU"))
        if productnameslen != productSKUlen:
            raise ValueError("Number of productNames and productSKU do not match")


def validate_request(requestschema):
    """decorator to validate request schema"""
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                requestschema(**request.json)
            except ValidationError as e:
                app.logger.error('{} - {}'.format(e, [422]))
                err_json = json.loads(e.json())
                abort(make_response(jsonify(err_json), 422))
            return func(*args, **kwargs)
        return wrapper
    return decorator

                    
@app.route("/recommendation", methods=["POST"])
@validate_request(RequestSchema)
def fusion_api():
    try:
        # get request contents
        req_content = request.json
        trd_content = req_content.get("trending") or None
        personalised = req_content.get("personalised")
        resultSize = req_content.get("resultSize") or default_resultSize
        storeid = req_content["storeId"]
        clientid = req_content.get("clientId")
        customerid = req_content.get("customerId")
        outletFilter = req_content.get("outletFilter") or False
        sku_list = req_content["productSKU"]
        display_score = req_content.get("displayScore")

        # personalized preprocess
        birthyear, postalsector, gender = personalised_none_handling(personalised)
        agegroup = None if birthyear is None else agegroup_map(birthyear, age_mapping)
        region = None if postalsector is None else region_map(postalsector, region_mapping)
        gender = None if gender not in ["m", "f"] else gender
        personalised_post = {"region": region, "age_group": agegroup, "gender": gender}
        
        # trending preprocess
        if trd_content:
            trd_birthyear = trd_content.get("birthYear")
            trd_age_group = None if trd_birthyear is None else agegroup_map(trd_birthyear, age_mapping)
            trd_content['age_group'] = trd_age_group
        
        requestid = str(time()).replace(".", "")
        model_req = {"storeid": storeid,
                     "resultSize": resultSize, 
                     "sku": sku_list, 
                     "customerId": customerid,
                     "outlet_filter": outletFilter,
                     "trending": trd_content,
                     "demo_info_dict": personalised_post}

        # if client-switching supported API
        if clientid is not None:
            api_urls = api_urls_all[clientid]
        else:
            api_urls = api_urls_all

        # send async requests to recommenders
        rre_weights = [i for i in req_content["weightage"] if i["weight"] != 0]
        n_threads = len(rre_weights)
        concat_list = Parallel(n_jobs=n_threads, backend="threading")(
            delayed(call_recommender)(
                api_urls, i["recommender"], i["weight"], model_req) for i in rre_weights)
        
        # check for any errors returned in recommenders called
        error_list, concat_list = format_errors(concat_list)
        
        # fuse results & return response
        results = duplicate_removal(concat_list)
        results = fusion(results)
        results = set_product_list_schema(results, requestid)

        # remove score unless indicated in request
        results_ = []
        if not display_score:
            for i in results:
                if i.get("score"):
                    del i["score"]
                results_.append(i)
            results = results_

        predicted_results = {"requestId": requestid,
                            "storeId": storeid,
                            "clientId": clientid,
                            "customerId": customerid,
                            "products": results}
        # if any errors in the recommenders, add the error message to the response
        if error_list:
            predicted_results['errorMessages'] = error_list

        return predicted_results

    except Exception:
        tb = traceback.format_exc()
        app.logger.error('{} - {}'.format(tb, [400]))
        return {"errorMessages": tb.replace("\n", "")}


@app.route("/insert-sku", methods=["POST"])
@validate_request(InsertSKUSimilaritySchema)
def insert_sku():
    try:
        req_content = request.json
        sku_list = req_content["productSKU"]
        clientid = req_content.get("clientId")
        storeid = req_content["storeId"]
        title = req_content["productNames"]
        insert_sku_req = {"storeid": storeid, "sku": sku_list, "title": title}
        if clientid is not None:
            api_urls = api_urls_all[clientid]
        else:
            api_urls = api_urls_all
        insert_sku_result = insert_sku_similarity(api_urls, insert_sku_req)
        return insert_sku_result
    
    except Exception:
        tb = traceback.format_exc()
        app.logger.error('{} - {}'.format(tb, [400]))
        try:
            return insert_sku_result
        except:
            return {"errorMessages": tb.replace("\n", "")}
    
    
if __name__ == "__main__":
    app.run(port=5000)
