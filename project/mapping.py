"""mapping conversions from request inputs"""
from datetime import date


def personalised_none_handling(dict_):
    """if personalised dict is None, handle the underlying key-values"""

    if dict_ == None:
        birthyear = postalsector = gender = None
    else:
        birthyear = dict_.get("birthYear")
        postalsector = dict_.get("postalSector")
        gender = dict_.get("gender")
    return birthyear, postalsector, gender


def agegroup_map(birthyear, age_mapping):
    """map birthyear to age group 1-7"""

    agegroup = None
    if len(str(birthyear)) == 4:
        current_year = date.today().year
        age = current_year - birthyear

        for age_group, age_limit in enumerate(age_mapping, 1):
            if age < age_limit:
                agegroup = str(age_group)
                break

    return agegroup


def region_map(postalsector, region_mapping):
    """map postal sector to region
    based on https://www.iproperty.com.sg/news/know-which-district-you-are-in-based-on-postal-code/
    """
    output = None
    for key, values in region_mapping.items():
        for item in values:
            if item == postalsector:
                output = key
                break
        else:
            continue
        break
    return output


if __name__ == "__main__":
    from time import time
    import json

    start = time()
    birthyear = 2013
    age_mapping = json.load(open("data/agegroup.json"))
    age_mapping = age_mapping["agegroup"]
    print(agegroup_map(birthyear, age_mapping))
    print(time()-start)

    # region_mapping = json.load(open("data/region.json"))
    # print(region_mapping)

    # start = time()
    # district_code = 53
    # print(region_map(district_code, region_mapping))
    # print(time()-start)

    # x = personalised_none_handling({"gender":"m"})
    # print(x)