import pandas as pd


def duplicate_removal(results):
    """remove duplicates from sku lists btw each recommender

    Args:
        results (list): 
            list of recommender results compiled from all model APIs
            format within each list ["recommender name", ["list of skus"], ["list of scores"], resultsize]
    """

    # form dataframe
    compile_ = []
    for result in results:
        rre = result[0]
        skus = result[1]
        scores = result[2]
        size = result[3]
        for sku, score in zip(skus, scores):
            compile_.append({"rre": rre, "sku": sku, "score": score, "result": size})
    df = pd.DataFrame(compile_)
    
    if len(df) != 0:
        # drop duplicates
        df = df.drop_duplicates(subset=["sku"], keep="last")

        compile_ = []
        recommenders = df["rre"].unique().tolist()
        for recommender in recommenders:
            skus = df[df["rre"] == recommender]["sku"].tolist()
            scores = df[df["rre"] == recommender]["score"].tolist()
            size = df[df["rre"] == recommender]["result"].tolist()[0]
            compile_.append((recommender, skus, scores, size))

        # if some rre is removed, add back with empty sku list
        if len(results) != len(compile_):
            rre1 = [rre for rre, _, _, _ in results]
            rre2 = [rre for rre, _, _, _ in compile_]
            diff = list(set(rre1).symmetric_difference(rre2))
            for rre in diff:
                for result in results:
                    rre_orginal = result[0]
                    size = result[3]
                    if rre == rre_orginal:
                        compile_.append((rre, [], [], size))

    return compile_


def fusion(results):
    """fusion logic for all recommenders' return"""
    # check if all recommenders have enough results
    surpluses = []
    for result in results:
        skus = result[1]
        actual = len(skus)
        expected = result[3]
        surplus = actual - expected
        surpluses.append(surplus)
    surplus_total = sum(surpluses)

    # some rre does not have enough results, & other rre cannot substitute
    if surplus_total <= 0:
        results = [(i[0], i[1], i[2]) for i in results]
        return results
    # all recommenders have more results then required
    elif all(i >= 0 for i in surpluses):
        results = [(i[0], i[1][:i[3]], i[2][:i[3]]) for i in results]
        return results
    # total is +ve, but have some negatives
    # assign surplus to reach expected results
    else:
        compile_ = []
        # add all the negatives
        total_negatives = abs(sum([i for i in surpluses if i < 0]))
        # for each positive assign minus down the negatives till 0 
            # slice sku_list by expected result size for each recommender
        for result in results:
            rre = result[0]
            skulist = result[1]
            scorelist = result[2]
            expected = result[3]
            sku_cnt = len(skulist)

            if sku_cnt == expected:
                compile_.append((rre, skulist, scorelist))
            else:
                # check if all rre have enough results
                if total_negatives != 0:
                    # check if rre has extra returns
                    if sku_cnt - expected > 0:
                        total = expected + total_negatives
                        # assign all extras to fill all missing ones, if possible
                        if sku_cnt >= total:
                            sku_sliced = skulist[:total]
                            score_sliced = scorelist[:total]
                            compile_.append((rre, sku_sliced, score_sliced))
                            total_negatives = 0
                        # assign max extras to fill missing ones
                        else:
                            compile_.append((result[0], skulist, scorelist))
                            total_negatives = total - sku_cnt
                else:
                    sku_sliced = skulist[:expected]
                    score_sliced = scorelist[:expected]
                    compile_.append((rre, sku_sliced, score_sliced))
        return compile_


def set_product_list_schema(results, requestid):
    """set products.key values into proper schema"""
    compile_ = []
    for result in results:
        rre = result[0]
        skus = result[1]
        scores = result[2]
        for cnt, zipped in enumerate(zip(skus, scores), 1):
            sku = zipped[0]
            score = zipped[1]
            uid = "{}-{}-{}".format(rre, requestid, cnt)
            compile_.append({"recommendationUID": uid, "sku": sku, "score": score})
    return compile_



