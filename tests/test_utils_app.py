import os
import sys
import pytest

from data import utils_data
from unittest.mock import patch


sys.path.append("project")

# add dummy configs
os.environ["API_URL"] = """{"asc":"x", "sml":"x", "ucf":"x", "adl":"x", "trd":"x"}"""
os.environ["RESULT_SIZE"] = "10"

from utils_app import call_recommender, format_errors


@patch("utils_app.requests.post")
@patch("utils_app.json.loads")
def test_call_recommender_with_error_messsage(mock_loads, mock_post):
    # Define the input arguments and expected output
    api_urls = {"sml": ""}
    recommender_nm = "sml"
    weight = 0.5
    data = {"resultSize": 2}
    expected_output = ("sml", "sku1", 0.8, 1, {"sml": "Traceback"})

    # Define the mocked response data and JSON content
    mock_response_data = {"sku": "sku1", "score": 0.8, "errorMessage": "Traceback"}
    mock_json_content = {"sku": "sku1", "score": 0.8, "errorMessage": "Traceback"}

    # Configure the mock objects
    mock_post.return_value.content = mock_response_data
    mock_loads.return_value = mock_json_content

    # Call the function being tested
    output = call_recommender(api_urls, recommender_nm, weight, data)

    # Verify the result
    assert output == expected_output


@pytest.mark.parametrize(
    "input_list, expected_output",
    [
        (utils_data.input1, utils_data.output1),
        (utils_data.input2, utils_data.output2),
        (utils_data.input3, utils_data.output3),
        (utils_data.input4, utils_data.output4),
    ]
)
def test_format_errors(input_list, expected_output):
    """Test format_errors function"""
    assert format_errors(input_list) == expected_output
