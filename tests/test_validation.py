import ast
import os
import sys

from data.request_schemas import *

sys.path.append("project")
# add dummy configs
os.environ["API_URL"] = '''{"asc":"x", "sml":"x", "ucf":"x", "adl":"x", "trd":"x"}'''
os.environ["RESULT_SIZE"] = "10"
from app import InsertSKUSimilaritySchema, RequestSchema


class Test_Trd:
    """schema testing for all trending related recommender"""

    def test_trending_w_all_inputs(self):
        assert RequestSchema(**trd_all_ok)

    def test_termAverage_not_2_int_in_list(self):
        try:
            _ = RequestSchema(**trd_termAverage)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "There must be 2 integers in the list"

    def test_trd_trending_key_missing(self):
        try:
            _ = RequestSchema(**trd_trending_key_missing)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "'trending' inputs are required if trd recommender called"

    def test_trd_allStores_missing(self):
        assert RequestSchema(**trd_allStores_missing)

    def test_trd_category_missing(self):
        try:
            _ = RequestSchema(**trd_category_missing)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "'category', 'date', 'termAverage' are required if trd recommender called"

    def test_trd_date_missing(self):
        try:
            _ = RequestSchema(**trd_date_missing)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "'category', 'date', 'termAverage' are required if trd recommender called"

    def test_trd_termAverage_missing(self):
        try:
            _ = RequestSchema(**trd_termAverage_missing)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "'category', 'date', 'termAverage' are required if trd recommender called"


class Test_datatype:
    """test all data type"""
    def test_postalSector_not_str(self):
        try:
            _ = RequestSchema(**postalSector_not_str)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "str type expected"

    def test_birthYear_not_int(self):
        try:
            _ = RequestSchema(**birthYear_not_int)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "value is not a valid integer"

    def test_gender_not_str(self):
        try:
            _ = RequestSchema(**gender_not_str)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "str type expected"

    def test_sku_list_not_list(self):
        try:
            _ = RequestSchema(**sku_not_list)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "value is not a valid list"

    def test_storeId_not_str(self):
        try:
            _ = RequestSchema(**storeId_not_str)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "str type expected"

    def test_weight_not_float(self):
        try:
            _ = RequestSchema(**weight_not_float)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "value is not a valid float"

    def test_customerId_not_str(self):
        try:
            _ = RequestSchema(**customerId_not_str)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "str type expected"

    def test_recommender_not_str(self):
        try:
            _ = RequestSchema(**recommender_not_str)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "str type expected"

    def test_allStores_not_bool(self):
        try:
            _ = RequestSchema(**allStores_not_bool)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "value could not be parsed to a boolean"

    def test_category_not_str(self):
        try:
            _ = RequestSchema(**category_not_str)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "str type expected"

    def test_date_not_str(self):
        try:
            _ = RequestSchema(**date_not_str)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "str type expected"

    def test_termAverage_not_list(self):
        try:
            _ = RequestSchema(**termAverage_not_list)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "value is not a valid list"

    def test_termAverage_list_not_contain_int(self):
        try:
            _ = RequestSchema(**termAverage_list_not_contain_int)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "value is not a valid integer"

    def test_trd_birth_year_not_int(self):
        try:
            _ = RequestSchema(**trd_birth_year_not_int)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "value is not a valid integer"
        
    def test_outletFilter_not_bool(self):
        try:
            _ = RequestSchema(**outletFilter_not_bool)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "value is not a valid boolean"


class Test_default_key_values:
    """test for keys which have default values"""

    def test_storeId_pass_if_missing(self):
        assert RequestSchema(**storeId_pass_if_missing)

    def test_requestSize_pass_if_missing(self):
        assert RequestSchema(**requestSize_pass_if_missing)


class Test_weightage:
    """all weightage validation"""
    def test_weightage_more_1(self):
        try:
            _ = RequestSchema(**weightage_more_1)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "sum of weightage is not equals to 1"

    def test_weightage_less_1_more_0(self):
        try:
            _ = RequestSchema(**weightage_less_1_more_0)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "sum of weightage is not equals to 1"

    def test_weightage_less_0(self):
        try:
            _ = RequestSchema(**weightage_less_0)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "ensure this value is greater than or equal to 0"


class Test_required_key_values:
    """test for all required keys"""
    def test_weight_missing(self):
        try:
            _ = RequestSchema(**weight_missing)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "field required"

    def test_productSKU_missing(self):
        try:
            _ = RequestSchema(**productSKU_missing)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "field required"

    def test_storeId_missing(self):
        try:
            _ = RequestSchema(**storeId_missing)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "field required"

    def test_recommender_missing(self):
        try:
            _ = RequestSchema(**recommender_missing)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "field required"


class Test_personalised:
    """test customerId presence if personlised algos called"""
    def test_ucf_present_no_customer_id(self):
        try:
            _ = RequestSchema(**ucf_present_no_customer_id)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "customerId required if ucf or adl recommender called"

    def test_adl_present_no_customer_id(self):
        try:
            _ = RequestSchema(**adl_present_no_customer_id)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "customerId required if ucf or adl recommender called"


class Test_insert_sku:
    """test insert sku schema validation"""
    def test_productNames_and_productSKU_different_number_of_items(self):
        try:
            _ = InsertSKUSimilaritySchema(**insert_sku_productNames_and_productSKU_different_number_of_items)
        except Exception as e:
            msg = ast.literal_eval(e.json())[0]["msg"]
        assert msg == "Number of productNames and productSKU do not match"


def test_recommender_nm_must_same_as_config():
    try:
        _ = RequestSchema(**recommender_nm_must_same_config)
    except Exception as e:
        msg = ast.literal_eval(e.json())[0]["msg"]
    assert msg == "'ascs' recommender is not in config list of ['asc', 'sml', 'ucf', 'adl', 'trd']"