import sys

import pytest

from data import fusion_data

sys.path.append("project")
from fusion import duplicate_removal, fusion, set_product_list_schema


@pytest.mark.parametrize("inputs, expected", [
     (fusion_data.results1, fusion_data.results1),
     (fusion_data.results4, fusion_data.results4_dup),
     (fusion_data.results5, fusion_data.results5_dup),
     (fusion_data.results7, fusion_data.results7_dup),
     ])
def test_duplicate_removal(inputs, expected):
    """test duplicates based on scenarios"""
    results = duplicate_removal(inputs)
    assert sorted(results) == sorted(expected)


@pytest.mark.parametrize("inputs, expected", [
     (fusion_data.results1, fusion_data.expected1),
     (fusion_data.results2, fusion_data.expected2),
     (fusion_data.results3, fusion_data.expected3),
     (fusion_data.results4, fusion_data.expected4),
     (fusion_data.results5, fusion_data.expected5),
     (fusion_data.results6, fusion_data.expected6),
     (fusion_data.results7, fusion_data.expected7),
     ])
def test_fusion_scenarios(inputs, expected):
    """test fusion scenarios, including 
    duplicate removal & insufficient results transfers"""
    x = duplicate_removal(inputs)
    results = fusion(x)
    assert sorted(results) == sorted(expected)


@pytest.mark.parametrize("inputs, expected", [
    (fusion_data.expected6, fusion_data.expected_schema6),
    (fusion_data.expected7, fusion_data.expected_schema7),
    ])
def test_product_schema_response(inputs, expected):
    requestid = "123456"
    results = set_product_list_schema(inputs, requestid)
    assert results == expected