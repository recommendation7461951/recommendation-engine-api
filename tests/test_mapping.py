import json
import sys

import pytest

sys.path.append("project")
from mapping import agegroup_map, region_map, personalised_none_handling


@pytest.mark.parametrize("inputs, expected", [
     (None, (None, None, None)),
     ({"gender": "m"}, (None, None, "m")),
     ({"birthYear": 1984, "gender": "m"}, (1984, None, "m")),
     ({"birthYear": 1984, "gender": "f"}, (1984, None, "f")),
     ({"birthYear": 1984, "gender": "f", "postalSector": "54"}, (1984, "54", "f")),
     ])
def test_personalised_none_handling(inputs, expected):
    """test func able to return None values in personalised request"""
    results = personalised_none_handling(inputs)
    assert results == expected


@pytest.mark.parametrize("inputs, expected", [
     (2010, "1"), (2000, "2"), (1990, "3"), (1983, "4"), 
     (1973, "5"), (1966, "6"), (1956, "7"), 
     (83, None), (0, None), (1850, None),
     ])
def test_agemapping(inputs, expected):
    """test birth year to age group mapping"""
    age_mapping = json.load(open("project/data/agegroup.json"))["agegroup"]
    results = agegroup_map(inputs, age_mapping)
    assert results == expected


@pytest.mark.parametrize("inputs, expected", [
     ("53", "north-east"), ("70", "north"), ("01", "central"),
     ("12", "west"), ("48", "east"), ("0123", None),
     ])
def test_regionmapping(inputs, expected):
    """test mapping of postal sector to region name"""
    region_mapping = json.load(open("project/data/region.json"))
    results = region_map(inputs, region_mapping)
    assert results == expected
