from typing import Dict

import pytest
import yaml
from pydantic import BaseModel, ValidationError, validator


class configSchema(BaseModel):
    resultSize: int
    api_url: Dict[str, str]

    @validator("api_url")
    def apiurldict(cls, v):
        print(v)
        for key in list(v.keys()):
            if key not in ["sml", "asc", "trd", "ucf", "adl", "sml-insert"]:
                raise ValueError("must be either sml, asc, trd, ucf, adl")
        return v


def test_config():
    """test for api key in api_url config"""
    cf = yaml.safe_load(open("project/config.yml"))
    out = configSchema(**cf)

    try:
        out = configSchema(**cf)
        print(out)
    except ValidationError as e:
        pytest.raises(e)