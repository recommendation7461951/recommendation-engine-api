# scenario 1 ------------
    # No duplicate
    # perfect weightage distribution
results1 = \
[('asc', ['1', '2', '3'], [0.99, 0.98, 0.97], 1),
 ('sml', ['4', '5'], [0.89, 0.88], 1),
 ('trd', ['6'], [0.7], 1),
 ('psn', ['7'], [0.6], 1)]

expected1 = \
[('asc', ['1'], [0.99]), 
 ('sml', ['4'], [0.89]), 
 ('trd', ['6'], [0.7]), 
 ('psn', ['7'], [0.6])]

# scenario 2 ------------
    # No duplicate
    # perfect weightage distribution
results2 = \
[('asc', ['1', '2', '3'], [0.99, 0.98, 0.97], 2),
 ('sml', ['4', '5'], [0.89, 0.88], 2),
 ('trd', ['6', '7'], [0.79, 0.78], 2),
 ('psn', ['8', '9'], [0.69, 0.68], 2)]

expected2 = \
[('asc', ['1', '2'], [0.99, 0.98]), 
 ('sml', ['4', '5'], [0.89, 0.88]), 
 ('trd', ['6', '7'], [0.79, 0.78]), 
 ('psn', ['8', '9'], [0.69, 0.68])]

# scenario 3 ------------
    # zeroed weightage (not applicable)
results3 = \
[('asc', ['1', '2', '3'], [0.99, 0.98, 0.97], 2),
 ('sml', ['4', '5'], [0.89, 0.88], 2)]

expected3 = \
[('asc', ['1', '2'], [0.99, 0.98]),
 ('sml', ['4', '5'], [0.89, 0.88])]

# scenario 4 ------------
    # Single duplicate
    # perfect weightage distribution
results4 = \
[('asc', ['1', '2', '3'], [0.99, 0.98, 0.97], 1),
 ('sml', ['3', '4'], [0.89, 0.88], 1),
 ('trd', ['5'], [0.79], 1),
 ('psn', ['6'], [0.69], 1)]

results4_dup = \
[('asc', ['1', '2'], [0.99, 0.98], 1),
 ('sml', ['3', '4'], [0.89, 0.88], 1),
 ('trd', ['5'], [0.79], 1),
 ('psn', ['6'], [0.69], 1)]

expected4 = \
 [('asc', ['1'], [0.99]), 
  ('sml', ['3'], [0.89]), 
  ('trd', ['5'], [0.79]), 
  ('psn', ['6'], [0.69])]

# scenario 5 ------------
    # multiple duplicate
    # perfect weightage distribution
results5 = \
[('asc', ['1', '2', '3'], [0.99, 0.98, 0.97], 1),
 ('sml', ['2', '3'], [0.89, 0.88], 1),
 ('trd', ['5'], [0.79], 1),
 ('psn', ['6'], [0.69], 1)]

results5_dup = \
[('asc', ['1'], [0.99], 1),
 ('sml', ['2', '3'], [0.89, 0.88], 1),
 ('trd', ['5'], [0.79], 1),
 ('psn', ['6'], [0.69], 1)]

expected5 = \
[('asc', ['1'], [0.99]), 
 ('sml', ['2'], [0.89]), 
 ('trd', ['5'], [0.79]), 
 ('psn', ['6'], [0.69])]

# scenario 6 ------------
    # No duplicate
    # Insufficient result from recommender algo
results6 = \
[('asc', ['1', '2'], [0.99, 0.98], 2),
 ('sml', ['3', '4', '5', '6'], [0.89, 0.88, 0.87, 0.86], 2),
 ('trd', ['7', '8', '9'], [0.79, 0.78, 0.77], 2),
 ('psn', ['10'], [0.6], 2)]

expected6 = \
[('asc', ['1', '2'], [0.99, 0.98]), 
 ('sml', ['3', '4', '5'], [0.89, 0.88, 0.87]), 
 ('trd', ['7', '8'], [0.79, 0.78]), 
 ('psn', ['10'], [0.6])]

expected_schema6 = \
[{'recommendationUID': 'asc-123456-1', 'sku': '1', 'score': 0.99}, 
 {'recommendationUID': 'asc-123456-2', 'sku': '2', 'score': 0.98}, 
 {'recommendationUID': 'sml-123456-1', 'sku': '3', 'score': 0.89}, 
 {'recommendationUID': 'sml-123456-2', 'sku': '4', 'score': 0.88}, 
 {'recommendationUID': 'sml-123456-3', 'sku': '5', 'score': 0.87}, 
 {'recommendationUID': 'trd-123456-1', 'sku': '7', 'score': 0.79}, 
 {'recommendationUID': 'trd-123456-2', 'sku': '8', 'score': 0.78}, 
 {'recommendationUID': 'psn-123456-1', 'sku': '10', 'score': 0.6}]


# scenario 7 ------------
    # Insufficient result from recommender algo or due to duplicates omissions
results7 = \
[('asc', ['1', '2'], [0.99, 0.98], 2),
 ('sml', ['1', '2', '3', '4'], [0.89, 0.88, 0.87, 0.86], 2),
 ('trd', ['5', '6', '7'], [0.79, 0.78, 0.77], 2),
 ('psn', ['8'], [0.6], 2)]

results7_dup = \
[('asc', [], [], 2),
 ('sml', ['1', '2', '3', '4'], [0.89, 0.88, 0.87, 0.86], 2),
 ('trd', ['5', '6', '7'], [0.79, 0.78, 0.77], 2),
 ('psn', ['8'], [0.6], 2)]

expected7 = \
[('asc', [], []), 
 ('sml', ['1', '2', '3', '4'], [0.89, 0.88, 0.87, 0.86]), 
 ('trd', ['5', '6', '7'], [0.79, 0.78, 0.77]), 
 ('psn', ['8'], [0.6])]

expected_schema7 = \
[{'recommendationUID': 'sml-123456-1', 'sku': '1', 'score': 0.89}, 
 {'recommendationUID': 'sml-123456-2', 'sku': '2', 'score': 0.88}, 
 {'recommendationUID': 'sml-123456-3', 'sku': '3', 'score': 0.87}, 
 {'recommendationUID': 'sml-123456-4', 'sku': '4', 'score': 0.86}, 
 {'recommendationUID': 'trd-123456-1', 'sku': '5', 'score': 0.79}, 
 {'recommendationUID': 'trd-123456-2', 'sku': '6', 'score': 0.78}, 
 {'recommendationUID': 'trd-123456-3', 'sku': '7', 'score': 0.77}, 
 {'recommendationUID': 'psn-123456-1', 'sku': '8', 'score': 0.6}]