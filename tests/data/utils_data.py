# Test case 1: sml and asc has no errors
input1 = [
    ("sml", ["sku1", "sku2"], [0.9, 0.8], 2, None),
    ("asc", ["sku1", "sku2"], [0.9, 0.8], 2, None),
]

output1 = (
    [],
    [("sml", ["sku1", "sku2"], [0.9, 0.8], 2),
     ("asc", ["sku1", "sku2"], [0.9, 0.8], 2)],
)

# Test case 2: sml has error, asc has no error
input2 = [
    ("sml", [], [], 2, {"sml": "Traceback"}),
    ("asc", ["sku1", "sku2"], [0.9, 0.8], 2, None),
]

output2 = (
    [{"sml": "Traceback"}],
    [("sml", [], [], 2),
     ("asc", ["sku1", "sku2"], [0.9, 0.8], 2)],
)

# Test case 3: Both recommenders have error
input3 = [
    ("sml", [], [], 2, {"sml": "Traceback"}),
    ("asc", [], [], 2, {"asc": "Traceback"}),
]

output3 = (
    [{"sml": "Traceback"}, {"asc": "Traceback"}],
    [("sml", [], [], 2), ("asc", [], [], 2)],
)

# Test case 4: 3 recommenders, 2 have error
input4 = [
    ("sml", [], [], 2, {"sml": "Traceback"}),
    ("asc", [], [], 2, {"asc": "Traceback"}),
    ("trd", ["sku1", "sku2"], [0.9, 0.8], 2, None),
]

output4 = (
    [{"sml": "Traceback"}, {"asc": "Traceback"}],
    [("sml", [], [], 2), ("asc", [], [], 2),
     ("trd", ["sku1", "sku2"], [0.9, 0.8], 2)],
)
