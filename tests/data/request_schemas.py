# personalised algo -----------

ucf_present_no_customer_id = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "weightage": [
        {
            "recommender": "ucf",
            "weight": 0.5
        },
        {
            "recommender": "asc",
            "weight": 0.5
        }
    ],
    "resultSize": 10
}

adl_present_no_customer_id = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "weightage": [
        {
            "recommender": "adl",
            "weight": 0.5
        },
        {
            "recommender": "asc",
            "weight": 0.5
        }
    ],
    "resultSize": 10
}

# recommender name -----------

recommender_nm_must_same_config = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "customerId": "111",
    "weightage": [
        {
            "recommender": "ascs",
            "weight": 1
        },
        {
            "recommender": "sml",
            "weight": 0
        }
    ]
}

# required fields ------------

recommender_missing = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "customerId": "111",
    "weightage": [
        {
            "weight": 1
        }
    ]
}

storeId_missing = \
{
    "productSKU": [
        "888"
    ],
    "customerId": "111",
    "weightage": [
        {
            "recommender": "asc",
            "weight": 1
        },
        {
            "recommender": "sml",
            "weight": 0
        }
    ]
}

productSKU_missing = \
{
    "storeId": "XXX",
    "customerId": "111",
    "weightage": [
        {
            "recommender": "asc",
            "weight": 1
        },
        {
            "recommender": "sml",
            "weight": 0
        }
    ]
}

weight_missing = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "customerId": "111",
    "weightage": [
        {
            "recommender": "asc",
            "weight": 0.5
        },
        {
            "recommender": "sml"
        }
    ],
    "resultSize": 10
}

# weightage ------------------
weightage_more_1 = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "customerId": "111",
    "weightage": [
        {
            "recommender": "asc",
            "weight": 1
        },
        {
            "recommender": "sml",
            "weight": 0.5
        }
    ],
    "resultSize": 6
}

weightage_less_1_more_0 = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "customerId": "111",
    "weightage": [
        {
            "recommender": "asc",
            "weight": 0.5
        },
        {
            "recommender": "sml",
            "weight": 0.3
        }
    ]
}

weightage_less_0 = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "customerId": "111",
    "weightage": [
        {
            "recommender": "asc",
            "weight": -1
        },
        {
            "recommender": "sml",
            "weight": 0
        }
    ]
}

# default values -------------

storeId_pass_if_missing = \
{
    "productSKU": [
    ],
    "storeId": "XXX",
    "resultSize": 10,
    "weightage": [
        {"recommender": "trd", "weight": 1}
    ],
    "trending": {
        "category": "all",
        "date": "2020-02-09",
        "termAverage": [3, 7]
    }
}

requestSize_pass_if_missing = \
{
    "productSKU": [
    ],
    "storeId": "XXX",
    "weightage": [
        {"recommender": "trd", "weight": 1}
    ],
    "trending": {
        "allStores": True,
        "category": "all",
        "date": "2020-02-09",
        "termAverage": [3, 7]
    }
}

# data type ------------------

postalSector_not_str = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "customerId": "111",
    "weightage": [
        {"recommender": "ucf", "weight": 0.5},
        {"recommender": "adl", "weight": 0.5}
    ],
    "resultSize": 10,
    "personalised": {
        "postalSector": ["12"],
        "birthYear": 1983,
        "gender": "m"
    }
}

birthYear_not_int = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "customerId": "111",
    "weightage": [
        {"recommender": "ucf", "weight": 0.5},
        {"recommender": "adl", "weight": 0.5}
    ],
    "resultSize": 10,
    "personalised": {
        "postalSector": "12",
        "birthYear": "nan",
        "gender": "m"
    }
}

gender_not_str = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "customerId": "111",
    "weightage": [
        {"recommender": "ucf", "weight": 0.5},
        {"recommender": "adl", "weight": 0.5}
    ],
    "resultSize": 10,
    "personalised": {
        "postalSector": "12",
        "birthYear": 1983,
        "gender": ["m"]
    }
}

sku_not_list =\
{
    "productSKU": 
        "888",
    "storeId": "XXX",
    "customerId": "111",
    "weightage": [
        {
            "recommender": "asc",
            "weight": 0.5
        },
        {
            "recommender": "sml",
            "weight": 0.5
        }
    ],
    "resultSize": 10
}

storeId_not_str = \
{
    "productSKU": [
        "888"
    ],
    "storeId": ["XXX"],
    "customerId": "111",
    "weightage": [
        {
            "recommender": "asc",
            "weight": 0.5
        },
        {
            "recommender": "sml",
            "weight": 0.5
        }
    ],
    "resultSize": 10
}

weight_not_float = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "customerId": "111",
    "weightage": [
        {
            "recommender": "asc",
            "weight": "b"
        },
        {
            "recommender": "sml",
            "weight": 11
        }
    ],
    "resultSize": 10
}

customerId_not_str = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "customerId": ["111"],
    "weightage": [
        {
            "recommender": "asc",
            "weight": "b"
        },
        {
            "recommender": "sml",
            "weight": 11
        }
    ],
    "resultSize": 10
}

recommender_not_str = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "customerId": ["111"],
    "weightage": [
        {
            "recommender": ["asc"],
            "weight": "b"
        },
        {
            "recommender": "sml",
            "weight": 11
        }
    ],
    "resultSize": 10
}

allStores_not_bool = \
{
    "productSKU": [
    ],
    "storeId": "XXX",
    "resultSize": 10,
    "weightage": [
        {"recommender": "trd", "weight": 1}
    ],
    "trending": {
        "allStores": "str",
        "category": "all",
        "date": "2020-02-09",
        "termAverage": [3, 7]
    }
}

category_not_str = \
{
    "productSKU": [
    ],
    "storeId": "XXX",
    "resultSize": 10,
    "weightage": [
        {"recommender": "trd", "weight": 1}
    ],
    "trending": {
        "allStores": True,
        "category": ["all"],
        "date": "2020-02-09",
        "termAverage": [3, 7]
    }
}

date_not_str = \
{
    "productSKU": [
    ],
    "storeId": "XXX",
    "resultSize": 10,
    "weightage": [
        {"recommender": "trd", "weight": 1}
    ],
    "trending": {
        "allStores": True,
        "category": "all",
        "date": ["2020-02-09"],
        "termAverage": [3, 7]
    }
}

termAverage_not_list = \
{
    "productSKU": [
    ],
    "storeId": "XXX",
    "resultSize": 10,
    "weightage": [
        {"recommender": "trd", "weight": 1}
    ],
    "trending": {
        "allStores": True,
        "category": "all",
        "date": "2020-02-09",
        "termAverage": 3
    }
}

termAverage_list_not_contain_int = \
{
    "productSKU": [
    ],
    "storeId": "XXX",
    "resultSize": 10,
    "weightage": [
        {"recommender": "trd", "weight": 1}
    ],
    "trending": {
        "allStores": True,
        "category": "all",
        "date": "2020-02-09",
        "termAverage": ["str", 7]
    }
}

outletFilter_not_bool = \
{
    "productSKU": [
        "888"
    ],
    "storeId": "XXX",
    "customerId": "111",
    "weightage": [
        {
            "recommender": "sml",
            "weight": 1
        }
    ],
    "resultSize": 10,
    "outletFilter": "True"
}    

# trending -------------------
trd_all_ok = {"productSKU": [
            ],
            "storeId": "x",
            "resultSize": 10,
            "weightage": [
                {"recommender": "trd", "weight": 1}
            ],
            "trending": {
                "allStores": True,
                "category": "all",
                "date": "2020-02-09",
                "termAverage": [3, 7]
            }
        }

trd_termAverage = {
        "productSKU": [
        ],
        "storeId": "XXX",
        "resultSize": 10,
        "weightage": [
            {"recommender": "trd", "weight": 1}
        ],
        "trending": {
            "allStores": True,
            "category": "all",
            "date": "2020-02-09",
            "termAverage": [3, 7, 5]
        }
    }

trd_trending_key_missing = \
{
    "productSKU": [],
    "storeId": "XXX",
    "resultSize": 10,
    "weightage": [
        {"recommender": "trd", "weight": 1}
    ]
}

trd_allStores_missing = \
{
    "productSKU": [
    ],
    "storeId": "XXX",
    "resultSize": 10,
    "weightage": [
        {"recommender": "trd", "weight": 1}
    ],
    "trending": {
        "category": "all",
        "date": "2020-02-09",
        "termAverage": [3, 7]
    }
}

trd_category_missing = \
{
    "productSKU": [
    ],
    "storeId": "XXX",
    "resultSize": 10,
    "weightage": [
        {"recommender": "trd", "weight": 1}
    ],
    "trending": {
        "date": "2020-02-09",
        "termAverage": [3, 7]
    }
}

trd_date_missing = \
{
    "productSKU": [
    ],
    "storeId": "XXX",
    "resultSize": 10,
    "weightage": [
        {"recommender": "trd", "weight": 1}
    ],
    "trending": {
        "category": "all",
        "termAverage": [3, 7]
    }
}

trd_termAverage_missing = \
{
    "productSKU": [
    ],
    "storeId": "XXX",
    "resultSize": 10,
    "weightage": [
        {"recommender": "trd", "weight": 1}
    ],
    "trending": {
        "category": "all",
        "date": "2020-02-09"
    }
}

trd_birth_year_not_int = {"productSKU": [
            ],
            "storeId": "x",
            "resultSize": 10,
            "weightage": [
                {"recommender": "trd", "weight": 1}
            ],
            "trending": {
                "allStores": True,
                "category": "all",
                "date": "2020-02-09",
                "termAverage": [3, 7],
                "birthYear": "nan"
            }
        }

insert_sku_productNames_and_productSKU_different_number_of_items = {
    "productSKU": [
        "MAIN-1", "MAIN-2"
    ],
    "productNames": ["hard boiled egg with sauce"],
    "storeId": "AMK631"
}